-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 24 Paź 2015, 19:49
-- Wersja serwera: 5.6.21
-- Wersja PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `lubcode`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `locals_ad`
--

CREATE TABLE IF NOT EXISTS `locals_ad` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `price` float NOT NULL,
  `is_deposit` tinyint(1) NOT NULL,
  `deposit` float NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `media` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `locals_ad`
--

INSERT INTO `locals_ad` (`id`, `user_id`, `name`, `description`, `price`, `is_deposit`, `deposit`, `x`, `y`, `media`) VALUES
(1, 5, 'Przykładowe ogłoszenie', 'Opis przykładowego ogłoszenia', 224, 0, 345, 1345, 5432, 'internet,garage,fridge,washer,'),
(2, 5, 'Przykładowe ogłoszenie', 'Opis przykładowego ogłoszenia', 224, 0, 345, 1345, 5432, 'internet,garage,fridge,washer,');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `status` int(11) NOT NULL,
  `ip` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `create` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `objects`
--

INSERT INTO `objects` (`id`, `name`, `x`, `y`, `status`, `ip`, `create`) VALUES
(4, 'ghtfhjshd', 63475, 74567500, 0, '::1', '2015-10-24 17:05:44');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `signup_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `last_ip` varchar(25) COLLATE utf8_polish_ci NOT NULL,
  `signup_ip` varchar(25) COLLATE utf8_polish_ci NOT NULL,
  `browser` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `activate_key` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `signup_date`, `last_login`, `last_ip`, `signup_ip`, `browser`, `activate_key`, `status`) VALUES
(5, 'daniel820', '65fa6ec749a3efbe9cf166aec4071271', 'daniel820@wp.pl', '2015-10-24 14:34:06', '2015-10-24 15:34:00', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36', '6ce5fa743c100533e37013674ad8749495fb8538', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `locals_ad`
--
ALTER TABLE `locals_ad`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `locals_ad`
--
ALTER TABLE `locals_ad`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `objects`
--
ALTER TABLE `objects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
