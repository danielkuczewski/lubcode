<section class="container3">
<?php
echo anchor('admin/suggest','Powrót');

?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src='http://maps.google.com/maps?file=api&amp;v=2.x&amp;sensor=false&amp;key=ABQIAAAAskA3kyDm631CGf6Rw_GrbBRBRXpdM9jp6G1MF9yLMfWuIYZt2BR5Ltrn1m4MP2hliyyWcC1AqLxZ3A' type='text/javascript'></script>

<body onload='mapaStart()' onunload='GUnload()'>
<br />
<script type='text/javascript'>
	<!--
		var mapa;
		var marker;

		var ikona = new GIcon();
		ikona.image = "http://maps.google.com/mapfiles/kml/pal2/icon10.png";
		ikona.iconAnchor = new GPoint(16, 16);
		ikona.shadow = "";
		ikona.infoWindowAnchor = new GPoint(16,16);
		ikona.iconSize = new GSize(32, 32);

		function pobierzWartosc(obiektRadio)
		{
			if(!obiektRadio)
				return null;
			var ilosc = obiektRadio.length;
			if(ilosc == undefined)
				if(obiektRadio.checked)
					return obiektRadio.value;
			else
				return null;
			for(var i=0; i<ilosc; i++)
			{
				if(obiektRadio[i].checked)
				{
					return obiektRadio[i].value;
				}
			}
			return null;
		}

		function mapaStart()
		{
			if(GBrowserIsCompatible())
			{
				mapa = new GMap2(document.getElementById("mapka"));

				mapa.setCenter(new GLatLng(<?php echo $object->x.','.$object->y;?>), 15, G_HYBRID_MAP);
				mapa.addControl(new GLargeMapControl());
				mapa.addControl(new GMapTypeControl());

				marker = new GMarker(mapa.getCenter(),{icon: ikona, draggable: true});
				marker.opis = 'bez opisu';
				mapa.addOverlay(marker);

				// zdarzenia dla markera
				GEvent.addListener(marker,'drag',uaktualnijWspolrzedne);
				GEvent.addListener(marker,'click',function()
				{
					marker.openInfoWindowHtml(marker.opis);
				});
				GEvent.trigger(marker,'drag');
			//	uaktualnijDymek()

				// zdarzenia dla mapy
				GEvent.addListener(mapa,'click',function(o,p)
				{
					if(p)
					{
						marker.setPoint(p);
						uaktualnijWspolrzedne();
					}
				});
			}
		}

		function uaktualnijWspolrzedne()
		{
			var input_lat = document.getElementById('lat');
			var input_lng = document.getElementById('lng');
			var punkt = marker.getLatLng();

			input_lat.value = punkt.lat();
			input_lng.value = punkt.lng();
		}


		function uaktualnijIkone()
		{
			var url = pobierzWartosc(document.forms['formularz'].elements['ikona']);
			marker.setImage(url);
			uaktualnijDymek()
		}


		function uaktualnijDymek()
		{
			var url = pobierzWartosc(document.forms['formularz'].elements['ikona']);
			var tytul = document.getElementById('nazwa').value;
			var opis = document.getElementById('opis').value;

			var html = '<img src="'+url+'" alt="" style="float: right; border: 0;" /><h3 class="marker">'+tytul+'</h3><p class="marker">'+opis+'</p>';
			marker.opis = html;
			if(mapa.getInfoWindow() && !mapa.getInfoWindow().isHidden())
				GEvent.trigger(marker,'click');
		}

	-->
	</script>
<div id="mapka" style="float: left; width: 500px; height: 500px; border: 1px solid black; background: gray;">
<!-- tu będzie mapa -->
	</div>
	<form method="post" id="formularz" style="display: block; width: 150px; margin-left: 520px;">
	<fieldset><legend>współrzędne</legend>
	<input type="text" class="disabled" readonly="readonly" id="lat" name="x" /><br /><span class="opis">długość geograficzna</span><br /><br />
	<input type="text" class="disabled" readonly="readonly" id="lng" name="y" /><br /><span class="opis">szerokość geograficzna</span><br />
    <!--Przeciągnij marker w żądane miejsce.--!>
	</fieldset>
	<fieldset><legend>dane miejsca</legend>
	<input onkeyup="uaktualnijDymek()" type="text" id="nazwa" name="placeholder" value="<?php echo  $object->name;?>" /><br /><span class="opis">nazwa</span><br /><br />
	<input onclick="uaktualnijIkone()" type="radio" name="ikona" id="ikona1" value="http://maps.google.com/mapfiles/kml/pal2/icon10.png" checked="checked" /><img style="border: 0;" alt="ikona1" src="http://maps.google.com/mapfiles/kml/pal2/icon10.png" />
	<input onclick="uaktualnijIkone()" type="radio" name="ikona" id="ikona2" value="http://maps.google.com/mapfiles/kml/pal2/icon11.png" /><img style="border: 0;" alt="ikona2" src="http://maps.google.com/mapfiles/kml/pal2/icon11.png" />
	<input onclick="uaktualnijIkone()" type="radio" name="ikona" id="ikona3" value="http://maps.google.com/mapfiles/kml/pal2/icon12.png"/><img style="border: 0;" alt="ikona3" src="http://maps.google.com/mapfiles/kml/pal2/icon12.png" /> <br />
	<input onclick="uaktualnijIkone()" type="radio" name="ikona" id="ikona4" value="http://maps.google.com/mapfiles/kml/pal2/icon13.png"/><img style="border: 0;" alt="ikona4" src="http://maps.google.com/mapfiles/kml/pal2/icon13.png" />
	<input onclick="uaktualnijIkone()" type="radio" name="ikona" id="ikona5" value="http://maps.google.com/mapfiles/kml/pal2/icon14.png"/><img style="border: 0;" alt="ikona5" src="http://maps.google.com/mapfiles/kml/pal2/icon14.png" /> <br />
	<span class="opis">ikona</span>
	</fieldset>
	<input type="submit" value="Wyślij propozycję!" />
	</form>
	<br style="clear: left" />
	<p id="info">Miejsce dodane przez użytkownika.</p>
<!--<div style="width:500px; height:500px; background:green;">Mapa z lokalizacją</div>-->
<?php
echo form_open('admin/suggest/'.$object->id);
echo '<select name="action">';
if($object->status == 0)
{
     echo '<option value="1">Akceptuj</option>';
}
echo '<option value="0">Usuń</option>';
echo '</select>';
echo form_submit('send','Wykonaj');
echo form_close();



?>
</section>