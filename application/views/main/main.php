
<section class="container">
  <?php echo anchor('main/add_ad','<div class="add_button">Dodaj ogłoszenie</div>'); ?>
  <?php echo anchor('main/search','<div class="search_button">Szukaj lokalu</div>'); ?>
</section>
<section class="container2">
<section class="map">
<div id="map"></div>
<script type="text/javascript">
var points = [
    <?php
    foreach($ads as $ad)
    {
         echo '{lat: '.$ad->x.', lng: '.$ad->y.', id: "'.$ad->id.'"},';
    }
    ?>
];

function initMap() {
  var myLatlng = {lat: -25.363, lng: 131.044};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 11,
    center: {lat:51.25, lng: 22.48}
  });
var marker = [];
<?php
foreach($ads as $ad)
{
echo "
  marker[".$ad->id."] = new google.maps.Marker({
    position: {lat:".$ad->x.", lng:".$ad->y."},
    map: map,
    title: '".$ad->id."',
    id: ".$ad->id."
  });

  map.addListener('center_changed', function() {
    // 3 seconds after the center of the map has changed, pan back to the
    // marker.
    window.setTimeout(function() {
      map.panTo(marker[".$ad->id."].getPosition());
    }, 3000);
  });

  marker[".$ad->id."].addListener('click', function() {
    window.location ='".site_url('main/show_ad')."'+'/'+marker[".$ad->id."].id;
  });
";}?>

}


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyUS1qDTuOTj956vANqwSKBUy9JMc1pAY&callback=initMap">
</script>
</section>
<section class="information">
<h2>Dodawaj ogłoszenia</h2>
<p>
W naszym serwisie Lublin Lokale dodajesz ogłoszenia zupełnie za darmo.
Dzięki naszym narzędziom możesz w prosty sposób dodać swoją nieruchomość do naszej bazy geograficznej
wybierając lokalizację jednym kliknięciem myszki. System automatycznie znajdzie ciekawe obiekty
w okolicy twojej nieruchomości.
</p>
</section>
<section class="information">
<h2>Znajdź nieruchomość dla siebie.</h2>
<p>
Oferujemy proste a zarazem bardzo skuteczne narzędzia w poszukiwaniu nowego lokalu.
Wystarczy zaznaczyć rejon na mapie jednym kliknięciem oraz określić jego wielkość aby
wyszukać nieruchomości w interesującej cię okolicy.
</p>
</section>
</section>

