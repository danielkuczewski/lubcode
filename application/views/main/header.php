<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="description" content="Lokale Lublin">
  <meta name="keywords" content="nieruchomości,lublin,wynajem lokali,stancja lublin,nieruchomości lublin">
  <meta name="author" content="Daniel Kuczewski & Robert Galiński">
  <meta name="generator" content="CoreEditor">
  <title>Lokale Lublin</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css');?>" type="text/css" media="all"  />
</head>
<body>
<header>
  <div class="logo">
       <?php echo anchor('','<img src="'.base_url('assets/img/logo.png').'"/>');?>
  </div>
  <div class="auth">
<?php
         if(!$userdata['is_logged']){echo anchor('auth/login','Logowanie');}
         else {echo anchor('account','Konto');}
         echo ' | ';
         echo anchor('','główna');
         echo ' | ';
         echo anchor('main/suggest','Zaproponuj obiekt');
?>
  </div>
</header>