<link rel="stylesheet" href="<?php echo base_url('assets');?>/dist/css/lightbox.css">
<section class="container3">
<table>
<tr><th>Tytuł ogłoszenia</th><th>Cena wynajmu</th><th>Kaucja</th><th>x</th><th>y</th></tr>
<?php
echo anchor('account/my_ads/','Powrót');
echo validation_errors();
echo form_open();
echo '<tr><td>';
echo form_input(array('name'=>'name','value'=>$ad->name,'placeholder'=>'Tytuł ogłoszenia'));
echo '</td><td>';
echo form_input(array('name'=>'price','value'=>$ad->price,'placeholder'=>'Cena wynajmu','type'=>'number','step'=>'0.01'));
echo '</td><td>';
echo form_input(array('name'=>'deposit','value'=>$ad->deposit,'placeholder'=>'Kaucja'));
echo '</td><td>';
echo form_input(array('name'=>'x','id'=>'x','value'=>$ad->x));
echo '</td><td>';
echo form_input(array('name'=>'y','id'=>'y','value'=>$ad->y));
echo '</td></tr></table>';?>
<div id="map"></div>
<script type="text/javascript">
function initMap() {
  var myLatLng = {lat: <?php echo $ad->x.', lng: '.$ad->y;?>};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: myLatLng
  });
  var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
        });
    google.maps.event.addListener(map, 'click', function(e) {
          setTimeout(placeMarker(e.latLng, map),2000);
        });
      function placeMarker(position, map) {
           marker.setMap(null);
        marker = new google.maps.Marker({
          position: position,
          map: map
        });
        setTimeout(map.panTo(position),200);
        var str = '';
        str += position;
        str = str.replace('(','');
        str = str.replace(')','');
        str = str.split(', ');
        document.getElementById('x').value=str[0];
        document.getElementById('y').value=str[1];
      }

}



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyUS1qDTuOTj956vANqwSKBUy9JMc1pAY&callback=initMap">
</script>

<br /></div>
<table><tr><td>
<?php
echo 'Opis ogłoszenia <br />';
echo form_textarea(array('name'=>'description','value'=>$ad->description,'placeholder'=>'Opis ogłoszenia'));
echo '</td><td><br />Internet';
echo form_checkbox('media[]','internet',$media['internet']);
echo '<br />Telewizja';
echo form_checkbox('media[]','tv',$media['tv']);
echo '<br />Garaż';
echo form_checkbox('media[]','garage',$media['garage']);
echo '<br />Piwnica';
echo form_checkbox('media[]','basement',$media['basement']);
echo '<br />Lodówka';
echo form_checkbox('media[]','fridge',$media['fridge']);
echo '<br />Pralka';
echo form_checkbox('media[]','washer',$media['washer']);
echo '<br />Kuchenka gazowa';
echo form_checkbox('media[]','cooker',$media['cooker']);
echo '<br />';
echo anchor('account/add_photo/'.$ad->id,'Dodaj zdjęcie');
echo '<br /><br /></td><td>';
if($galery[0] > ' ')
{
  foreach($galery as $g)
  {
       echo '<a class="example-image-link" href="'.base_url('assets/upload/'.$ad->user_id.'/'.$g).'"
       data-lightbox="example-set" data-title=""><img class="example-image"  style="height:10em;"
       src="'.base_url('assets/upload/'.$ad->user_id.'/'.$g).'" alt=""/></a>';
  }
}

echo form_submit(array('name'=>'send','value'=>'Edytuj ogłoszenie','class'=>'button'));
echo '</td></td></table>';
echo form_close();
?>
</section>
<script src="<?php echo base_url('assets');?>/dist/js/lightbox-plus-jquery.min.js"></script>
