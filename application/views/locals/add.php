<section class="container3">
<h2>Dodaj ogłoszenie</h2>
<?php
echo validation_errors();
echo form_open_multipart();
echo '<table><tr><th>Tytuł ogłoszenia</th><th>Cena wynajmu</th><th>Kaucja</th><th>x</th><th>y</th></tr><tr><td>';
echo form_input(array('name'=>'name','value'=>set_value('name'),'placeholder'=>'Tytuł ogłoszenia'));
echo '</td><td>';
echo form_input(array('name'=>'price','value'=>set_value('price'),'placeholder'=>'Cena wynajmu','type'=>'number','step'=>'0.01'));
echo '</td><td>';
echo form_input(array('name'=>'deposit','value'=>set_value('deposit'),'placeholder'=>'Kaucja'));
echo '</td><td>';
echo form_input(array('name'=>'x','id'=>'x','value'=>set_value('x')));
echo '</td><td>';
echo form_input(array('name'=>'y','id'=>'y','value'=>set_value('y')));
echo '</td></tr></table><br />';?>
<div id="map"></div>
<script type="text/javascript">
function initMap() {
  var myLatLng = {lat: 51.309, lng: 22.596};
var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
        });
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: myLatLng
  });

    google.maps.event.addListener(map, 'click', function(e) {
          placeMarker(e.latLng, map);
        });
      function placeMarker(position, map) {
            marker.setMap(null);
        marker = new google.maps.Marker({
          position: position,
          map: map
        });
        map.panTo(position);
        var str = '';
        str += position;
        str = str.replace('(','');
        str = str.replace(')','');
        str = str.split(', ');
        document.getElementById('x').value=str[0];
        document.getElementById('y').value=str[1];
      }

}



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyUS1qDTuOTj956vANqwSKBUy9JMc1pAY&callback=initMap">
</script>
<table><tr><td>
<?php
echo 'Opis ogłoszenia <br />';
echo form_textarea(array('name'=>'description','value'=>set_value('description'),'placeholder'=>'Opis ogłoszenia'));
echo '</td><td><br />Internet';
echo form_checkbox('media[]','internet',TRUE);
echo '<br />Telewizja';
echo form_checkbox('media[]','tv');
echo '<br />Garaż';
echo form_checkbox('media[]','garage');
echo '<br />Piwnica';
echo form_checkbox('media[]','basement');
echo '<br />Lodówka';
echo form_checkbox('media[]','fridge');
echo '<br />Pralka';
echo form_checkbox('media[]','washer');
echo '<br />Kuchenka gazowa';
echo form_checkbox('media[]','cooker');
echo '<br />
<br />';
echo form_input(array('name'=>'userfile','type'=>'file'));
echo '</td></tr></table></div><br /><br />';
echo form_submit(array('name'=>'send','value'=>'Dodaj ogłoszenie','class'=>'button'));
echo form_close();
?>
</section>

