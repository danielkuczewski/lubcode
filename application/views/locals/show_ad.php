<link rel="stylesheet" href="<?php echo base_url('assets');?>/dist/css/lightbox.css">
<section class="container3">

<?php
echo anchor('main/search','Powrót');
echo '<section class="ad">
<div class="map">
<h2>'.$ad->name.'</h2>
<p>
'.$ad->description.'
</p>
<p>
'.anchor('main/question/'.$ad->user_id.'/'.$ad->id,'Pytanie do właściciela').'
</p>
<div class="galery">
';
if($galery[0] > ' ')
{
foreach($galery as $g)
{
     echo '<a class="example-image-link" href="'.base_url('assets/upload/'.$ad->user_id.'/'.$g).'"
     data-lightbox="example-set" data-title=""><img class="example-image"
     src="'.base_url('assets/upload/'.$ad->user_id.'/'.$g).'" alt="" style="height:10em;"/></a>';
}
}
echo '
</div>

</div>
<div class="information">
<h1>'.$ad->price.'zł / mies</h1>
<h3>Kaucja '.$ad->deposit.' zł</h3>
<ul>';
     if($media['internet']){ echo '<li>Internet</li>';}
     if($media['tv']){ echo '<li>Telewizja</li>';}
     if($media['garage']){ echo '<li>Garaż</li>';}
     if($media['basement']){ echo '<li>Piwnica</li>';}
     if($media['fridge']){ echo '<li>Lodówka</li>';}
     if($media['washer']){ echo '<li>Pralka</li>';}
     if($media['cooker']){ echo '<li>Kuchenka</li>';}


?>
</ul>
<h3>W pobliżu</h3>
<ul id="lista">
</ul>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>
<script>
var lista = "";
<?php
foreach($near as $n)
{
     echo 'lista += "<li>'.$n->name.' "+xsi(new google.maps.LatLng('.$ad->x.','.$ad->y.'),new google.maps.LatLng('.$n->x.','.$n->y.'))+" km</li>";';
     //echo '<li>'.$n['name'].' - '.round($n['distance']).' m</li>';
}
?>
document.getElementById('lista').innerHTML = lista;

function xsi(p1,p2)
{
  //calculates distance between two points in km/s
  function calcDistance(p1, p2){
    return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
}
return calcDistance(p1, p2);
}
</script>
</div>
</section>
</section>
</section>
<script src="<?php echo base_url('assets');?>/dist/js/lightbox-plus-jquery.min.js"></script>