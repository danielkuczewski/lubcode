<section class="container3">
<table style="width:100%;">
     <tr>
          <th>tytuł</th>
          <th>cena</th>
          <th></th>
     </tr>
<?php
if(count($ads) > 0)
{
     foreach($ads as $ad)
     {
          $odll=sqrt(pow(cos(pi()*set_value('y')/180)*($ad->x-set_value('x')),2)+pow($ad->y-set_value('y'),2))*pi()*1257.274*25/9;
           if($odll<=$this->input->post('radius')){
          echo '<tr>
          <td>'.$odll.' m</td>
          <td>'.$ad->name.'</td>
          <td>'.$ad->price.' zł/mies</td>
          <td>'.anchor('main/show_ad/'.$ad->id,'Zobacz ofertę').'</td>
          </tr>';
        }

     }

}
else
{
     echo '<tr><td colspan="3">Brak wyników wyszukiwania</td></tr>';
}

?>
</table>
<h2>Wyszykiwarka lokali</h2>
<p>Aby wyszukać lokalizcję zaznacz na mapie interesujący cię obszar wybierz jego zakres oraz wpisz dodatkowe interesujące cię parametry.</p>
<table>
<tr><th>Cena minimalna</th><th>Cena maksymalna</th><th>Promień (wielkość obszaru m )</th><th>x</th><th>y</th></tr>
<?php
echo validation_errors();
echo form_open();
echo '<tr><td>';
echo form_input(array('name'=>'price1','placeholder'=>'Cena minimalna','value'=>'0','type'=>'number','step'=>'0.01'));
echo '</td><td>';
echo form_input(array('name'=>'price2','placeholder'=>'Cena maksymalna','value'=>'10000','type'=>'number','step'=>'0.01'));
echo '</td><td>';
echo form_input(array('name'=>'radius','placeholder'=>'Promień (wielkość obszaru m )','value'=>'3000','type'=>'number','step'=>'0'));
echo '</td><td>';
echo form_input(array('name'=>'x','id'=>'x','placeholder'=>'x','value'=>set_value('x'),'type'=>'double'));
echo '</td><td>';
echo form_input(array('name'=>'y','id'=>'y','placeholder'=>'y','value'=>set_value('y'),'type'=>'double'));
echo '</td></tr></table>';?>
<div id="map"></div>
<script type="text/javascript">
function initMap() {
  var myLatLng = {lat: 51.309, lng: 22.596};
var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
        });
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: myLatLng
  });

    google.maps.event.addListener(map, 'click', function(e) {
          placeMarker(e.latLng, map);
        });
      function placeMarker(position, map) {
           marker.setMap(null);
        marker = new google.maps.Marker({
          position: position,
          map: map
        });
        map.panTo(position);
        var str = '';
        str += position;
        str = str.replace('(','');
        str = str.replace(')','');
        str = str.split(', ');
        document.getElementById('x').value=str[0];
        document.getElementById('y').value=str[1];
      }

}



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyUS1qDTuOTj956vANqwSKBUy9JMc1pAY&callback=initMap">
</script>
<br /><br />
<?php
echo form_submit(array('name'=>'send','value'=>'Szukaj ofert','class'=>'button'));
echo form_close();

?>
</section>