<section class="container3">
<h2>Przypomnienie hasła</h2>
<p>
Zapomniałeś hasła?<br />
Nie ma problemu. Podaj poniżej swój adres email, na który zostało zarejestrowane twoje konto
a my wygenerujemy nowe hasło i wyślemy je do ciebie.
</p>
<?php
echo form_open();
echo form_input(array('name'=>'email','class'=>'input1','placeholder'=>'Twój adres E-mail'));
echo validation_errors();
echo "<br /><br />";
echo form_submit(array('name'=>'send','value'=>'Wyślij nowe hasło','class'=>'button'));
echo form_close();
?>
</section>