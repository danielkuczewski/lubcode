<section class="container3">
<?php
if($success)
{
     echo '<h2>Gratulacje</h2>
     <p>Twoje konto właśnie zostało aktywowane. Teraz możesz się zalogować <a href="'.site_url().'/auth/login">Tutaj</a></p>';
}
else
{
     echo '
          <p><b>Błąd!<br /></b>
          Twoje konto niestety nie zostało aktywowane.<br />
          Możliwe przyczyny:
          <ul>
                  <li>Konto jest już aktywne</li>
                  <li>Konto zostało zablokowane przez administracje</li>
                  <li>Rejestrowałeś/aś się dawniej niż 48 godzin temu</li>
          </ul>
          </p>
     ';

}
?>
</section>