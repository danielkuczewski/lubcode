<section class="container3">
<h2>Logowanie</h2>
<p>
    Masz już konto?<br />
    Podaj swój login i hasło aby się zalogować. Jeśli nie pamiętasz hasła możesz skorzystać z opcji przypomnienia hasła.
    Jeśli jeszcze nie masz konta w serwisie <?php echo anchor('auth/register','Zarejestruj się');?>
</p>

<?php
echo form_open();
echo validation_errors();
if($error != FALSE){ echo '<br /><b>Logowanie nieudane.<br /> '.$error.'.</b><br />';}
echo form_input(array('name'=>'username','class'=>'input1','placeholder'=>'login'));
echo '<br /><br />';
echo form_password(array('name'=>'password','class'=>'input1','placeholder'=>'hasło'));
echo '<br /><br />';
echo form_submit(array('name'=>'send','value'=>'Zaloguj się','class'=>'button'));
echo '<br /><br />';
echo anchor('auth/remind','Przypomnij hasło');
echo '<br /><br />';
echo form_close();



?>
</section>

