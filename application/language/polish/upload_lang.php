<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['upload_userfile_not_set'] = 'Nie można znaleźć pliku o podanej nazwie.';
$lang['upload_file_exceeds_limit'] = 'Przesłany plik przekracza doswoloną wielkość.';
$lang['upload_file_exceeds_form_limit'] = 'Plik jest za duży.';
$lang['upload_file_partial'] = 'Plik został przesłany tylko częściowo.';
$lang['upload_no_temp_directory'] = 'Brakuje folderu tymczasowego.';
$lang['upload_unable_to_write_file'] = 'Plik nie może być zapisany na dysku.';
$lang['upload_stopped_by_extension'] = 'Plik nie został przesłany ze względu na rozszerzenie.';
$lang['upload_no_file_selected'] = 'Nie wybrałeś pliku do przesłania.';
$lang['upload_invalid_filetype'] = 'Próbujesz przesłać niedozwolony typ pliku.';
$lang['upload_invalid_filesize'] = 'Plik, który chcesz przesłać jest za duży.';
$lang['upload_invalid_dimensions'] = 'Obraz, który próbujesz wgrać nie pasuje do wymaganych wymiarów.';
$lang['upload_destination_error'] = 'Wystąpił problem podczas przesyłania pliku.';
$lang['upload_no_filepath'] = 'Niepoprawna ścieżka uploaudu.';
$lang['upload_no_file_types'] = 'Nie podano dozwolonych typów plików.';
$lang['upload_bad_filename'] = 'Nazwa pliku już istnieje na serwerze.';
$lang['upload_not_writable'] = 'Folder docelowy nie jest zapisywalny.';
