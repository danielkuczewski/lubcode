<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

    public function index()
    {
        $r['ads'] = $this->loc->list_all_ads();
        $this->my_view('main/main',$r);
    }

    public function add_ad()
    {
          if(!$this->user->userdata['is_logged'])
          {
               //niezalogowany
               $this->my_view('locals/add_no_logged');
          }
          else
          {
               if($this->form_validation->run('add_ad'))
               {
                    $media = array();

                    $this->loc->add_ad(
                           $this->user->userdata['data']->user_id,
                           $this->input->post('name'),
                           $this->input->post('description'),
                           $this->input->post('price'),
                           $this->input->post('deposit'),
                           $this->input->post('x'),
                           $this->input->post('y'),
                           $this->input->post('media')
                    );
                   $this->my_view('main/success');
               }
               else
               {
                   $this->my_view('locals/add');
               }

          }
    }

    public function search()
    {
         $r['ads'] = array();
         if($this->form_validation->run('search_ad'))
         {
              $r['ads'] = $this->loc->search_ad(
                $this->input->post('price1'),
                $this->input->post('price2'),
                $this->input->post('radius'),
                $this->input->post('x'),
                $this->input->post('y')
              );
         }
         $this->my_view('locals/search',$r);
    }

    public function show_ad($id = null)
    {
         $r['ad'] = $this->loc->get_ad($id);
         $r['media'] = explode(',',$r['ad']->media);
         $r['galery'] = explode(' ',$r['ad']->galery);
         $r['near'] = $this->loc->near($id);

          $m1 = array('internet'=>false,'tv'=>false,'garage'=>false,'basement'=>false,'fridge'=>false,'washer'=>false,'cooker'=>false);
          foreach($r['media'] as $m)
          {
               $m1[$m] = TRUE;
          }
          $r['media'] = $m1;
         $this->my_view('locals/show_ad',$r);
    }

    public function suggest()
    {
         $this->load->helper('captcha');
         $this->form_validation->set_rules('name','nazwa obiektu','required');
         $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_re_captcha');
         if($this->form_validation->run())
         {
              $this->loc->send_suggest($this->input->post('name'),$this->input->post('x'),$this->input->post('y'));
              $this->my_view('main/success');
         }
         else
         {
              $this->my_view('locals/suggest');
         }
    }

    public function question($id = null,$ad_id = null)
    {
         if($this->form_validation->run('question'))
         {
              $this->user->send_question(
                  $this->input->post('user_id'),
                  $this->input->post('ad_id'),
                  $this->input->post('email'),
                  $this->input->post('content')
              );
              $this->my_view('main/success');
         }
         else
         {
             $r['user'] = $this->user->show_user($id);
             $r['ad'] = $this->loc->get_ad($ad_id);
             $r['user_ads'] = $this->loc->get_user_ads($id);
             $this->my_view('main/question',$r);
         }
    }

}