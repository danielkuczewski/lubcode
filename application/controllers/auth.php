<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends MY_Controller
{
     function __construct()
     {
          parent::__construct();
          $this->data['userdata'] = $this->user->userdata;
          if($this->user->userdata['is_logged'])
          {
               redirect('account/index','refresh');
          }
     }

     function check_email1($email)
     {
         if($this->db->get_where('users',array('email'=>$email))->num_rows() == 0)
          {
               return FALSE;
          }
          else
          {
               return TRUE;
          }
     }



     function register()
     {

          $this->load->helper('captcha');
          $this->data['title'] = "Rejestracja nowego użytkownika.";
          $this->load->view('main/header',$this->data);
          if($this->form_validation->run('signup') == FALSE)
          {
               $this->load->view('auth/register');
          }
          else
          {
               $this->user->register($this->input->post('username'),$this->input->post('email'),$this->input->post('password'));
               $this->load->view('auth/success');
          }
          $this->load->view('main/footer');
     }

     function activate_account($username,$key)
     {
          $this->data['title'] = "Aktywacja konta.";
          $this->load->view('main/header',$this->data);
          $this->data['success'] = $this->user->activate_account($username,substr($key,0,100));
          $this->load->view('auth/activate',$this->data);
          $this->load->view('main/footer');
     }

     function login()
     {
          $this->data['error'] = FALSE;
          $this->form_validation->set_rules('username','login','required');
          $this->form_validation->set_rules('password','hasło','required');
          if($this->form_validation->run() == TRUE)
          {
               $r = $this->user->login($this->input->post('username'),$this->input->post('password'));
              if($r == 0)
              {
                   redirect('account/index','refresh');
              }
              else
              {
                   if($r == 1){$this->data['error'] = 'Brak loginu w bazie.';}
                   if($r == 2){$this->data['error'] = 'Niepoprawne hasło.';}
                   if($r == 3){$this->data['error'] = 'Konto nieaktywne. ';}
                   if($r == 4){$this->data['error'] = 'Konto zablokowane';}
              }
          }
          $this->data['title'] = "Logowanie użytkownika.";
          $this->load->view('main/header',$this->data);
          $this->load->view('auth/login_form',$this->data);
          $this->load->view('main/footer');
     }

     function remind()
     {
          $this->data['title'] = "Przypomnienie hasła.";
          $this->form_validation->set_rules('email','email','trim|required|valid_email|max_length[100]|callback_check_email1');
          $this->load->view('main/header',$this->data);
          if($this->form_validation->run())
          {
               $this->user->remind_password($this->input->post('email'));
               $this->load->view('auth/remind_ok');
          }
          else
          {
               $this->load->view('auth/remind');
          }
          $this->load->view('main/footer');
     }
}

?>