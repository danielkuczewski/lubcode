<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MY_Controller
{
     function __construct()
     {
          parent::__construct();
          parent::admin_auth();
     }

     function index()
     {
          $this->my_view('admin/panel');
     }

     function suggest($id = null)
     {
          $this->form_validation->set_rules('send','x','required');
          if($this->form_validation->run())
          {
               if($this->input->post('action') == 1)
               {
                    $this->loc->accept_suggest($id);
               }
               if($this->input->post('action') == 0)
               {
                    $this->loc->delete_object($id);
               }
          }
          $r['list1'] = $this->loc->list_suggest_to_accept();
          $r['list2'] = $this->loc->list_accepted_suggest();
          $this->my_view('admin/suggest',$r);
     }

     function show_suggest($id)
     {
          $r['object'] = $this->loc->get_object($id);
          $this->my_view('admin/show_object',$r);
     }
}


?>