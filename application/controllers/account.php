<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Account extends MY_Controller
{
     function __construct()
     {
          parent::__construct();
          parent::user_auth();
     }

     function index()
     {
          $this->my_view('account/panel');
     }

     function check_password($password)
     {
         if($this->db->get_where('users',array('password'=>substr(sha1($password),0,32),'username'=>$this->user->userdata['username']))->num_rows() == 0)
          {
               return FALSE;
          }
          else
          {
               return TRUE;
          }
     }

     function edit_account()
     {
          $this->form_validation->set_rules('password','Hasło','required');
          $this->form_validation->set_rules('password1','Powtórz hasło','required|matches[password]');
          $this->form_validation->set_rules('password2','Stare hasło','required|callback_check_password');
          $this->data['ok'] = FALSE;
          if($this->form_validation->run() == TRUE)
          {
               $this->data['ok'] = $this->user->edit_account($this->input->post('password'));
          }

          $this->my_view('account/edit',$this->data);
     }

     function logout()
     {
          $this->user->logout();
          redirect('','refresh');
     }

     function my_ads()
     {
          $r['ads'] = $this->loc->get_user_ads($this->user->userdata['data']->user_id);
          $this->my_view('account/my_ads',$r);
     }

     function delete_ad($id)
     {
          $this->form_validation->set_rules('ad_id','x','required');
          if($this->form_validation->run())
          {
               $this->loc->delete_ad($this->input->post('ad_id'));
               $this->my_view('main/success');
          }
          else
          {
               $r['ad'] = $this->loc->get_ad($id);
               $this->my_view('account/delete_ad',$r);
          }
     }

     function edit_ad($id)
     {
         if($this->form_validation->run('add_ad'))
         {
              $this->loc->edit_ad(
              $id,
              $this->input->post('name'),
              $this->input->post('description'),
              $this->input->post('price'),
              $this->input->post('deposit'),
              $this->input->post('x'),
              $this->input->post('y'),
              $this->input->post('media')
              );
         }

          $r['ad'] = $this->loc->get_ad($id);
          $r['media'] = explode(',',$r['ad']->media);
          $m1 = array('internet'=>false,'tv'=>false,'garage'=>false,'basement'=>false,'fridge'=>false,'washer'=>false,'cooker'=>false);
          foreach($r['media'] as $m)
          {
               $m1[$m] = TRUE;
          }
          $r['media'] = $m1;
          $r['galery'] = explode(' ',$r['ad']->galery);
          $this->my_view('account/edit_ad',$r);
     }

     function add_photo($id)
     {
          $this->form_validation->set_rules('send','x','required');
          if($this->form_validation->run())
          {
               $this->loc->add_photo($id);
          }
          $r['ad'] = $this->loc->get_ad($id);
          $r['galery'] = explode(' ',$r['ad']->galery);
          $this->my_view('account/add_photo',$r);
     }
}

?>