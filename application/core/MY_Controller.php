<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
      public $data = [];

      function __construct()
      {
           parent::__construct();
           $this->data['userdata'] = $user['u'] = $this->user->userdata;
      }
     function admin_auth()
     {

     }

     function user_auth()
     {
          $this->data['userdata'] = $this->user->userdata;
          if(!$this->user->userdata['is_logged'])
          {
               redirect('auth/login','refresh');
          }
     }

      function my_view($location,$data = null)
      {
           $this->load->view('main/header',$this->data);
           $this->load->view($location,$data);
           $this->load->view('main/footer');
      }

     function my_view_admin($name,$view,$data2 = null)
     {
          $this->data['title'] = $name;
          $this->load->view('admin/header',$this->data);
          $this->load->view('admin/'.$view,$data2);
          $this->load->view('admin/footer');
     }

     function my_view_account($name,$view,$data2 = null)
     {
          $r = array();
          $r['username'] = $this->user->userdata['data']->username;
          $this->data['title'] = $name;
          $this->load->view('main/header',$this->data);
          $this->load->view('account/header');
          $this->load->view('account/sumary',$r);
          $this->load->view('account/menu',$r);
          $this->load->view('account/'.$view,$data2);
          $this->load->view('account/footer');
          $this->load->view('main/footer');
     }


      function check_username($username)
      {
          if($this->db->get_where('users',array('username'=>$username))->num_rows() > 0)
          {
             return FALSE;
          }
          else
          {
             return TRUE;
          }
      }

      function check_username1($username)
      {
          if($this->db->get_where('users',array('username'=>$username))->num_rows() == 0)
          {
             return FALSE;
          }
          else
          {
             return TRUE;
          }
      }

     function check_email($email)
     {
         if($this->db->get_where('users',array('email'=>$email))->num_rows() > 0)
          {
               return FALSE;
          }
          else
          {
               return TRUE;
          }
     }

     function re_captcha($cap='')
     {
            $google_url="https://www.google.com/recaptcha/api/siteverify";
            $secret='6LfYTgsTAAAAACSAmv6EeVvD5IK5UU-OlXGV6DNg';
            $ip=$_SERVER['REMOTE_ADDR'];
            $url=$google_url."?secret=".$secret."&response=".$cap."&remoteip=".$ip;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  //important for localhost!!!
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
            $res = curl_exec($curl);
            curl_close($curl);
            $res= json_decode($res, true);
            //reCaptcha success check
            if($res['success'])
            {
              return TRUE;
            }
            else
            {
              return FALSE;
            }
     }
}



?>