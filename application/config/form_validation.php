<?php
$config = array(
    'signup'=> array(
            array(
               'field'=>'username',
               'label'=>'Login',
               'rules'=>'trim|required|min_length[3]|max_length[25]|alpha_dash|callback_check_username'
            ),
            array(
               'field'=>'email',
               'label'=>'E-mail',
               'rules'=>'trim|required|valid_email|max_length[100]|callback_check_email'
            ),
            array(
               'field'=>'email2',
               'label'=>'Powtórz E-mail',
               'rules'=>'trim|required|matches[email]'
            ),
            array(
               'field'=>'password',
               'label'=>'Hasło',
               'rules'=>'required|min_length[6]'
            ),
            array(
               'field'=>'password2',
               'label'=>'Powtórz hasło',
               'rules'=>'required|matches[password]'
            ),
            array(
               'field'=>'g-recaptcha-response',
               'label'=>'Captcha',
               'rules'=>'callback_re_captcha'
            )
          ),
    'admin_add_user'=> array(
           array(
               'field'=>'username',
               'label'=>'Login',
               'rules'=>'trim|required|min_length[3]|max_length[25]|alpha_dash|callback_check_username'
            ),
            array(
               'field'=>'email',
               'label'=>'E-mail',
               'rules'=>'trim|required|valid_email|max_length[100]|callback_check_email'
            ),
            array(
               'field'=>'email2',
               'label'=>'Powtórz E-mail',
               'rules'=>'trim|required|matches[email]'
            ),
            array(
               'field'=>'password',
               'label'=>'Hasło',
               'rules'=>'required|min_length[6]'
            ),
            array(
               'field'=>'password2',
               'label'=>'Powtórz hasło',
               'rules'=>'required|matches[password]'
            )
    ),
    'edit_user'=> array(
           array(
               'field'=>'username',
               'label'=>'Login',
               'rules'=>'trim|required|min_length[3]|max_length[25]|alpha_dash|callback_check_username'
            ),
            array(
               'field'=>'email',
               'label'=>'E-mail',
               'rules'=>'trim|required|valid_email|max_length[100]|callback_check_email'
            ),
            array(
               'field'=>'email2',
               'label'=>'Powtórz E-mail',
               'rules'=>'trim|required|matches[email]'
            ),
            array(
               'field'=>'password',
               'label'=>'Hasło',
               'rules'=>'min_length[6]'
            ),
            array(
               'field'=>'password2',
               'label'=>'Powtórz hasło',
               'rules'=>'matches[password]'
            )
    ),
    'add_ad'=> array(
            array(
               'field'=>'name',
               'label'=>'tytuł',
               'rules'=>'required'
               ),
            array(
               'field'=>'price',
               'label'=>'Cena',
               'rules'=>'required|greater_than[10]'
            ),
            array(
               'field'=>'x',
               'label'=>'współrzędna x',
               'rules'=>'required|greater_than[10]'
               ),
            array(
               'field'=>'y',
               'label'=>'współrzędna y',
               'rules'=>'required|greater_than[10]'
            )
    ),
    'search_ad'=>array(
          array(
            'field'=>'price1',
            'label'=>'cena minimalna',
            'rules'=>'required'
          ),
          array(
            'field'=>'price2',
            'label'=>'cena maksymalna',
            'rules'=>'required'
          ),
          array(
            'field'=>'radius',
            'label'=>'wielkość obszaru',
            'rules'=>'required|greater_than[1]'
          ),
          array(
            'field'=>'x',
            'label'=>'wsp x',
            'rules'=>'required|greater_than[1]'
          ),
          array(
            'field'=>'y',
            'label'=>'wsp y',
            'rules'=>'required|greater_than[1]'
          ),
    ),
    'question'=>array(
          array(
               'field'=>'email',
               'label'=>'E-mail',
               'rules'=>'trim|required|valid_email'
          ),
          array(
               'field'=>'content',
               'label'=>'treść wiadomości',
               'rules'=>'trim|required'
          ),
          array(
               'field'=>'g-recaptcha-response',
               'label'=>'Captcha',
               'rules'=>'callback_re_captcha'
          )
    )

);




?>