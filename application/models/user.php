<?php

class User extends CI_Model
{
     public $userdata = array('data'=>null,'username'=>null,'is_logged'=>False);

     function __construct()
     {
          parent::__construct();
          $q = $this->db->get_where('users',array('username'=>$this->session->userdata('username')));
          if($this->session->userdata('is_logged'))
          {
               $q = $this->db->get_where('users',array('username'=>$this->session->userdata('username')));
               $this->userdata['data'] = $q->result()[0];
          }
          $this->userdata['username'] = $this->session->userdata('username');
          $this->userdata['is_logged'] = $this->session->userdata('is_logged');
     }

     /////////////////////////////
     /// User functions //////////
     /////////////////////////////
     function register($username,$email,$password,$admin = null)
     {
        //create activate key
        $key = sha1(date('U'));
        $link = site_url().'/auth/activate_account/'.$username.'/'.$key;
        $new_user = array(
          'user_id'=>null,
          'username'=>$username,
          'password'=>sha1($password),
          'email'=>$email,
          'signup_date'=>date('Y-m-d H:i:s'),
          'signup_ip'=>$_SERVER['REMOTE_ADDR'],
          'browser'=>$_SERVER['HTTP_USER_AGENT'],
          'activate_key'=>$key,
          'status'=>0
        );

        $this->db->insert('users',$new_user);
        if($admin == null)
        {
            $this->load->library('email');
            $this->email->from('daniel820@wp.pl','daniel820@wp.pl');
            $this->email->to($email);
            $this->email->subject('Rejestracja');
            $this->email->message('Aby ukończyć rejestrację kliknij link aktywacyjny: '.$link);

            $this->email->send();
        }
     }

     function activate_account($username,$key)
     {
         $q = $this->db->get_where('users',array('username'=>$username,'activate_key'=>$key,'status'=>0));
         if($q->num_rows() > 0)
         {
              $this->db->where('username',$username)->update('users',array('status'=>'1'));
              return TRUE;
         }
         else
         {
              return FALSE;
         }
     }

     function login($username,$password)
     {
           $q1 = $this->db->get_where('users',array('username'=>$username));
           if($q1->num_rows() == 0)
           {
                return 1;
           }
           else
           {
                $q2 = $this->db->get_where('users',array('username'=>$username,'password'=>substr(sha1($password),0,32)));
                if($q2->num_rows() == 0)
                {
                     return 2;
                }
                else
                {
                     $r = $q2->result()[0];
                     if($r->status == 0)
                     {
                          return 3;
                     }
                     else if($r->status == 2)
                     {
                          return 4;
                     }
                     else
                     {
                          $this->db->where('username',$username)->update('users',array('last_login'=>date('Y-m-d H:i:s')));
                          $this->session->set_userdata(array('username'=>$username,'is_logged'=>TRUE));
                          return 0;
                     }
                }
           }
     }

     function logout()
     {
         $this->session->unset_userdata('username');
         $this->session->set_userdata('is_logged',FALSE);
     }

     function remind_password($email)
     {
          $new_pass = substr(sha1(date('u')),0,8);
          $new_pass1 = substr(sha1($new_pass),0,32);
          $this->db->where('email',$email)->update('users',array('password'=>$new_pass1));
          $this->load->library('email');
          $this->email->from('daniel820@wp.pl','daniel820@wp.pl');
          $this->email->to($email);
          $this->email->subject('Przypomnienie hasła');
          $this->email->message('Twoje nowe hasło: '.$new_pass);
          $this->email->send();
     }

     function edit_account($new_password)
     {
              $this->db->where('username',$this->userdata['username'])->update('users',array('password'=>sha1($new_password)));
              return TRUE;
     }

     function delete_account($user_id)
     {
              $this->db->where('user_id',$user_id)->delete('users');
     }

     //////////////////////////
     ////  Admin functions  ///
     //////////////////////////

     function user_stats()
     {
          $q = $this->db->get('users');
          $q1 = $this->db->get_where('users',array('last_login >'=>date('Y-m-d',(date('U'))-2592000)));
          $q2 = $this->db->get_where('users',array('signup_date'=>date('Y-m-d')));
          $q3 = $this->db->get_where('users',array('signup_date'=>date('Y-m-d',(date('U'))-86400)));
          $q4 = $this->db->get_where('users',array('signup_date >='=>date('Y-m-d',(date('U'))-604800)));
          $q5 = $this->db->get_where('users',array('signup_date >='=>date('Y-m-d',(date('U'))-2592000)));
          $r['total'] = $q->num_rows();
          $r['active'] = $q1->num_rows();
          $r['today'] = $q2->num_rows();
          $r['yesterday'] = $q3->num_rows();
          $r['week'] = $q4->num_rows();
          $r['month'] = $q5->num_rows();
          return $r;
     }
     function users_list($filtr)
     {
          $filtr1 = array(
              'username'=>$filtr['username'],
              'email'=>$filtr['email'],
              'signup_ip'=>$filtr['signup_ip'],
              'last_ip'=>$filtr['last_ip']
          );
          $filtr2 = array(
              'signup_date <='=>$filtr['signup_date1'],
              'signup_date >='=>$filtr['signup_date2'],
              'last_login <='=>$filtr['last_login1'],
              'last_login >='=>$filtr['last_login2']
          );
         $r = $this->db->where($filtr2)->like($filtr1)->get('users');
         return $r;
     }

     function show_user($user_id)
     {
         $q = $this->db->get_where('users',array('user_id'=>$user_id));
         foreach($q->result() as $r)
         {
              return $r;
         }
         return FALSE;
     }

     function edit_user($user_id,$username,$email,$password)
     {
          if($password != '')
          {
               $pass = substr(sha1($password),0,32);
               $this->db->where('user_id',$user_id)->update('users',array('username'=>$username,'email'=>$email,'password'=>$pass));
          }
          else
          {
               $this->db->where('user_id',$user_id)->update('users',array('username'=>$username,'email'=>$email));
          }
     }
     function login_to_user($user_id)
     {
           $q = $this->db->get_where('users',array('user_id'=>$user_id));
           $r = $q->result()[0];
           $this->session->set_userdata(array('username'=>$r->username,'is_logged'=>TRUE));
           return TRUE;
     }

     function what_user_id($username)
     {
          $q = $this->db->get_where('users',array('username'=>$username));
          if($q->num_rows() > 0)
          {
               $r = $q->result()[0];
               return $r->user_id;
          }
          else
          {
               return -1;
          }
     }

     function what_username($user_id)
     {
          if($user_id == 0)
          {
               $username = 'Sh7';
          }
          else
          {
               $q = $this->db->get_where('users',array('user_id'=>$user_id));
               $username = $q->result()[0]->username;
          }

          return $username;
     }

     function send_question($user_id,$ad_id,$email_q,$content)
     {
          $user = $this->show_user($user_id);
          $ad= $this->db->get_where('locals_ad',array('id'=>$ad_id))->result()[0];

          $this->load->library('email');
          $this->email->from($email_q,'Lokale lublin');
          $this->email->to($user->email);
          $this->email->subject('Pytanie o ogłoszenie');
          $contents = 'Pytanie z serwisu Lokale Lublin dotyczące ogłoszenia:
          '.$ad->name.'
          Treść wiadomości:
          ';
          $contents .= $content;
          $this->email->message($contents);

          $this->email->send();
     }
}

?>