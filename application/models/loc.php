<?php
class Loc extends User
{
     function __construct()
     {
          parent::__construct();
     }

     function send_suggest($name,$x,$y)
     {
          $ip = $_SERVER['REMOTE_ADDR'];
          $this->db->insert('objects',array('name'=>$name,'x'=>$x,'y'=>$y,'status'=>0,'ip'=>$ip,'create'=>date('Y-m-d H:i:s')));
     }

     function list_suggest_to_accept()
     {
          $q = $this->db->get_where('objects',array('status'=>'0'));
          return $q->result();
     }

     function list_accepted_suggest()
     {
          $q = $this->db->get_where('objects',array('status'=>'1'));
          return $q->result();
     }

     function accept_suggest($id)
     {
          $q = $this->db->get_where('objects',array('status'=>0));
          if($q->num_rows() > 0)
          {
               $this->db->where('id',$id)->update('objects',array('status'=>'1'));
          }
     }

     function get_object($id)
     {
          return $this->db->get_where('objects',array('id'=>$id))->result()[0];
     }

     function delete_object($id)
     {
          $this->db->delete('objects',array('id'=>$id));
          return TRUE;
     }

     function add_ad($user_id,$name,$description,$price,$deposit,$x,$y,$media)
     {
          if(!file_exists('./assets/upload/'.$user_id))
          {
             mkdir('./assets/upload/'.$user_id);
          }
          $config['upload_path'] = './assets/upload/'.$user_id;
          $config['allowed_types'] = 'gif|jpg|png';
          $config['max_size']    = '10000';
          $this->upload->initialize($config);
          $str = '';
          if(count($media) > 0)
          {
            foreach($media as $m)
            {
                 $str .= $m.',';
            }
          }
          $media = $str;

          if($this->upload->do_upload())
          {
              $data = $this->upload->data();
              $this->db->insert('locals_ad',array(
              'user_id'=>$user_id,
              'name'=>$name,
              'description'=>$description,
              'price'=>$price,
              'deposit'=>$deposit,
              'x'=>$x,
              'y'=>$y,
              'media'=>$media,
              'galery'=>trim($data['file_name'])
                ));
              return TRUE;
          }
          else
          {
               $this->db->insert('locals_ad',array(
              'user_id'=>$user_id,
              'name'=>$name,
              'description'=>$description,
              'price'=>$price,
              'deposit'=>$deposit,
              'x'=>$x,
              'y'=>$y,
              'media'=>$media
                ));
                return TRUE;
          }
     }

     function add_photo($ad_id)
     {

          $p = $this->db->get_where('locals_ad',array('id'=>$ad_id))->result()[0];
          if(!file_exists('./assets/upload/'.$p->user_id))
          {
             mkdir('./assets/upload/'.$user_id);
          }
          $config['upload_path'] = './assets/upload/'.$p->user_id;
          $config['allowed_types'] = 'gif|jpg|png';
          $config['max_size']    = '10000';
          $this->upload->initialize($config);


          if($this->upload->do_upload())
          {
              $data = $this->upload->data();
              $p->galery .= ' '.$data['file_name'];

              $this->db->where('id',$ad_id)->update('locals_ad',array('galery'=>trim($p->galery)));
              return TRUE;
          }
     }

     function search_ad($price1,$price2,$radius,$x,$y)
     {
        $q = $this->db->get_where('locals_ad',array(
        'price <'=>$price2,
        'price >'=>$price1,
        'y >'=>($y-$radius),
        'y <'=>($y+$radius),
        'x <'=>($x+$radius),
        'x >'=>($x-$radius)));
        return $q->result();
     }

     function get_ad($id)
     {
          $q = $this->db->get_where('locals_ad',array('id'=>$id));
          return $q->result()[0];
     }

     function get_user_ads($user_id)
     {
          $q = $this->db->get_where('locals_ad',array('user_id'=>$user_id));
          return $q->result();
     }

     function delete_ad($id)
     {
          $this->db->delete('locals_ad',array('id'=>$id));
          return TRUE;
     }

     function edit_ad($ad_id,$name,$description,$price,$deposit,$x,$y,$media)
     {
          $str = '';
          foreach($media as $m)
          {
               $str .= $m.',';
          }
          $media = $str;
         $this->db->where(array('id'=>$ad_id,'user_id'=>$this->user->userdata['data']->user_id))->update('locals_ad',array(
              'name'=>$name,
              'description'=>$description,
              'price'=>$price,
              'deposit'=>$deposit,
              'x'=>$x,
              'y'=>$y,
              'media'=>$media
         ));
         return TRUE;
     }

     function list_all_ads()
     {
          return $this->db->get('locals_ad')->result();
     }

     function near($id)
     {
          $q = $this->db->get_where('locals_ad',array('id'=>$id))->result()[0];
          $q1 = $this->db->get_where('objects',array(
              'x <'=>($q->x+1),
              'x >'=>($q->x-1),
              'y <'=>($q->y+1),
              'y >'=>($q->y-1)
          ));
          /*$w = array();
          $i = 0;
          if(count($q1) > 0)
          {
              foreach($q1->result() as $r)
              {
                 $o = sqrt(pow(($q->x-$r->x),2)+pow(($q->y-$r->y),2)) * 15000;
                 //$o = sqrt(pow(cos(pi()*$q->y/180)*($r->x-$q->x),2))+pow($r->y-$q->y,2)*pi()*(318921.85/9);
                 $w[$i] = array('name'=>$r->name,'distance'=>$o);
                 $i++;
              }
          }
          return $w;*/
          return $q1->result();

     }
}


?>